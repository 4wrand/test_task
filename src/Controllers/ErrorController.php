<?php

namespace Controllers;

use Core\Controller\AController;

class ErrorController extends AController 
{
    public function error404() 
    {
        $this->view('404.twig', ['title' => '404 Page not found']);
    }
}