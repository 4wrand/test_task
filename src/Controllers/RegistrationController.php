<?php 

namespace Controllers;

use Core\Controller\AController;
use Core\Database\DB;
use Core\Redirect;
use Core\PostMessage;
use Core\Doctrine;
use Core\Verification;
use Models\User;

class RegistrationController extends AController
{
    public function default() 
    {
        if (isset($_SESSION['isAuth']) && $_SESSION['isAuth']) {
            Redirect::redirectTo();
            return;
        }
        return $this->view('registration.twig');
    }

    public function registration (array $post) 
    {
        if (!$this->checkExistsUserRegistrationData($post)) {
            PostMessage::message('Required data for registration is missing', 'warning');
            Redirect::redirectToBack();
            return;
        }

        if (!$this->verifyRegistrationData($post)) {
            Redirect::redirectToBack();
            return;
        }

        $user = $this->getUserRepository()->findOneBy(['email' => $email, 'anonymous' => false]);

        if ($user) {
            PostMessage::message('Email already exists', 'warning');
            Redirect::redirectToBack();

            return;
        } else {
            $user = new User();
        }

        $preparedData = $this->prepareRegistrationData($post);

        $em = Doctrine::getEntityManager();
 
        $user->setEmail($preparedData['email']);
        $user->setFirstName($preparedData['firstName']);
        $user->setLastName($preparedData['lastName']);
        $user->setType(DEFAULT_USER_TYPE);
        $user->setAnonymous(false);
        $user->setPassword($preparedData['password']);

        try {
            $em->persist($user);
            $em->flush();
        } catch (\Exeption $e) {
            PostMessage::message($e->getMessage, 'warning');
            Redirect::redirectToBack();
        }
        
        PostMessage::message('Registration completed successfully', 'successful');
        Redirect::redirectTo('auth');
    }

    private function checkExistsUserRegistrationData(array $userData): bool
    {
        if (!empty($userData['email']) 
            && !empty($userData['firstName'])
            && !empty($userData['lastName'])
            && !empty($userData['password'])
            && !empty($userData['confirmPassword'])
        ) {
            return true;
        }

        return false;

    }

    private function verifyRegistrationData(array $userData): bool
    {
        $registrationData = [];

        if (Verification::checkEmail($userData['email'])) {
            $registrationData['email'] = true;
        } else {
            PostMessage::message('Incorrect email field', 'warning');
        }
        
        if (Verification::checkUserName($userData['firstName'])
            && Verification::checkUserName($userData['lastName'])
        ) {
            $registrationData['firstName'] = true;
        } else {
            PostMessage::message('Incorrect first or last name field', 'warning');
        }
          
        if ($userData['password'] === $userData['confirmPassword']) {
            if (Verification::checkPassword($userData['password'])) {
                $registrationData['password'] = true;
            } else {
                PostMessage::message('Incorrect password field', 'warning');
            }
        } else {
            PostMessage::message('Passwords do not match', 'warning');
        }

        foreach ($registrationData as $value) {
            if ($value === false){
                return false;
            }
        }

        return true;
    }

    private function prepareRegistrationData(array $userData): array
    {
        $preparedData = [];

        $preparedData['email'] = trim($userData['email']);
        $preparedData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT);
        $preparedData['firstName'] = $this->prepareUserName($userData['firstName']);
        $preparedData['lastName'] = $this->prepareUserName($userData['lastName']);

        return $preparedData;
    }

    private function prepareUserName(string $name): string
    {
        $name = mb_strtolower($name);
        $name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");

        return $name;
    }

    private function getUserRepository()
    {
        $em = Doctrine::getEntityManager();

        return $em->getRepository('\\Models\\User');
    }
}       