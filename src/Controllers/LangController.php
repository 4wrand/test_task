<?php

namespace Controllers;

use Core\Config;
use Core\Redirect;
use Core\Controller\AController;

class LangController extends AController
{   
    public function switchLang () 
    {
        $_SESSION['lang'] = !empty($_GET['lang']) ? $_GET['lang'] : null;

        Redirect::redirectToBack();
    }
}