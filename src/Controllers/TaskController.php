<?php

namespace Controllers;

use Core\Config;
use Core\Redirect;
use Core\Controller\AController;
use Core\Doctrine;
use Core\Verification;
use Core\PostMessage;
use Models\Task;
use Models\User;
use Repository\TaskRepository;

class TaskController extends AController
{   

    public function default() {
        $sort = !empty($_SESSION['sorting']['sort']) ? $_SESSION['sorting']['sort'] : 'id';
        $order = !empty($_SESSION['sorting']['order']) ? $_SESSION['sorting']['order'] : 'asc';
        $numberOfPage = !empty($_GET['page']) ? $_GET['page'] : 1;

        $tasks = TaskRepository::getAllTasksByCriteria($sort, $order, $numberOfPage);
        $numberOfPages = TaskRepository::countNumberPages();

        $this->view('index.twig', [
            'tasks'         => $tasks,
            'numberOfPages' => $numberOfPages,
            'numberOfPage'  => $numberOfPage,
            'sort'          => $sort,
            'order'         => $order,
        ]);
    }

    public function setSortForTaskList(array $post)
    {   
        if (!empty($_SESSION['sorting']['order']) && $_SESSION['sorting']['sort'] === $post['sort']) {
            $_SESSION['sorting']['order'] = $_SESSION['sorting']['order'] === 'asc' ? 'desc' : 'asc';
        } else {
            $_SESSION['sorting']['order'] = 'asc';
        }

        $_SESSION['sorting']['sort'] = $post['sort'];

        Redirect::redirectToBack();
    }

    public function createTask(array $post) 
    {
        if (!$this->checkExistsTaskData($post)) {
            PostMessage::message('Required data for create task is missing', 'warning');
            Redirect::redirectToBack();

            return;
        }

        if (!$this->verifyTaskData($post)) {
            Redirect::redirectToBack();

            return;
        }

        $em = Doctrine::getEntityManager();
        $preparedData = $this->prepareTaskData($post);

        if (!empty($_SESSION['isAuth']) && !empty($_SESSION['userId'])) {
            $user = $this->getUserRepository()->find($_SESSION['userId']);
        } else {

            $user = $this->getUserRepository()->findOneBy(['email' => $preparedData['email'], 'anonymous' => false]);

            if ($user) {
                if (!$user->isAnonymous()) {
                    PostMessage::message('To created a task with this email, you need to register', 'warning');
                    Redirect::redirectToBack();

                    return;
                }
            } else {
                $user = new User();
                $user->setEmail($preparedData['email']);
                $user->setFirstName($preparedData['firstName']);

                try {
                    $em->persist($user);
                    $em->flush();
                } catch (\Exeption $e) {
                    PostMessage::message($e->getMessage, 'warning');
                    Redirect::redirectToBack();
                    
                    return;
                }
            }
        }

        $preparedData = $this->prepareTaskData($post);

        $task = new Task();
        $task->setContent($post['task']);
        $task->setAuthor($user);

        try {
            $em->persist($task);
            $em->flush();
        } catch (\Exeption $e) {
            PostMessage::message($e->getMessage, 'warning');
            Redirect::redirectToBack();
        }

        PostMessage::message('Task successfully created', 'successful');
        Redirect::redirectToBack();
    }

    public function editTask() 
    {
        if ($this->checkEditAccess()) {
            $taskId = !empty($_GET['task_id']) ? $_GET['task_id'] : null;
            $task = $this->getTaskRepository()->find($taskId);

            $this->view('edit_task.twig', [
                'task' => $task,
            ]);

        } else {
            PostMessage::message('Access denied for these action', 'warning');
            Redirect::redirectToBack();
            return;
        }
    }

    public function saveChange(array $post)
    {
        if ($this->checkEditAccess()) {
            $taskId = !empty($_POST['taskId']) ? $_POST['taskId'] : null;

            if (!$taskId) {
                PostMessage::message('Not enough data to change', 'warning');
                Redirect::redirectToBack();
                return;
            }

            $task = $this->getTaskRepository()->find($taskId);

            if ($task) {
                $em = Doctrine::getEntityManager();

                $oldContent = $task->getContent();
                $content = !empty($_POST['content']) ? $_POST['content'] : $oldContent;
                if ($oldContent !== $content) {
                    $task->setEdited(true);
                }
                $task->setContent(htmlspecialchars($content, ENT_QUOTES));
                $task->setCompleted(
                    !empty($_POST['complete']) ? true : false
                );

                try {
                    $em->persist($task);
                    $em->flush();
                } catch (\Exeption $e) {
                    PostMessage::message($e->getMessage, 'warning');
                    Redirect::redirectToBack();
                }

                PostMessage::message('Task changed successfully', 'successful');
                Redirect::redirectTo();
                return;
            }

        } else {
            PostMessage::message('Access denied for these action', 'warning');
            Redirect::redirectToBack();
            return;
        } 
    }

    private function checkEditAccess(): bool
    {
        if (!empty($_SESSION['isAuth']) && !empty($_SESSION['userId'])) {
            $user = $this->getUserRepository()->find($_SESSION['userId']);

            if ($user->getType() === 'admin') {
                return true;
            } 
        }

        return false;
    }

    private function checkExistsTaskData(array $userData): bool
    {
        if (!empty($userData['email']) 
            && !empty($userData['firstName'])
            && !empty($userData['task'])
        ) {
            return true;
        }

        return false;
    }

    private function verifyTaskData(array $userData): bool
    {
        $registrationData = [];

        if (Verification::checkEmail($userData['email'])) {
            $registrationData['email'] = true;
        } else {
            PostMessage::message('Incorrect email field', 'warning');
        }
        
        if (Verification::checkUserName($userData['firstName'])) {
            $registrationData['firstName'] = true;
        } else {
            PostMessage::message('Incorrect first name field', 'warning');
        }

        foreach ($registrationData as $value) {
            if ($value === false){
                return false;
            }
        }

        return true;
    }

    private function prepareTaskData(array $userData): array
    {
        $preparedData = [];

        $preparedData['email'] = trim($userData['email']);
        $preparedData['firstName'] = $this->prepareUserName($userData['firstName']);
        $preparedData['task'] = htmlspecialchars($userData['task'], ENT_QUOTES);

        return $preparedData;
    }

    private function prepareUserName(string $name): string
    {
        $name = mb_strtolower($name);
        $name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");

        return $name;
    }

    private function getUserRepository()
    {
        $em = Doctrine::getEntityManager();

        return $em->getRepository('\\Models\\User');
    }

    private function getTaskRepository()
    {
        $em = Doctrine::getEntityManager();

        return $em->getRepository('\\Models\\Task');
    }
}