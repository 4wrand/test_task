<?php 

namespace Controllers;

use Core\Controller\AController;
use Core\Database\DB;
use Core\Redirect;
use Core\PostMessage;
use Core\Doctrine;
use Models\User;

class AuthController extends AController
{
    public function default()
    {
        if (isset($_SESSION['isAuth']) && $_SESSION['isAuth']) {
            Redirect::redirectTo();
        }
        $this->view('login.twig');
    }

    public function login(array $post)
    {    
        if (!empty($post['email']) && !empty($post['password'])) {
            $user = $this->getUserRepository()->findOneBy(['email' => $post['email'], 'anonymous' => false]);

            if ($user && password_verify($post['password'], $user->getPassword())) {
                $_SESSION['isAuth'] = true;
                $_SESSION['userId'] = $user->getId();

                Redirect::redirectTo();
            } else {
                PostMessage::message('Invalid email or password', 'warning');
                Redirect::redirectToBack();
            }
            
        } else {
            PostMessage::message('E-mail or password data was not found', 'warning');
            Redirect::redirectToBack();
        }
    }

    public function logout()
    {
        unset($_SESSION['isAuth'], $_SESSION['user']);
        Redirect::redirectTo();
    }

    private function getUserRepository()
    {
        $em = Doctrine::getEntityManager();

        return $em->getRepository('\\Models\\User');
    }
}