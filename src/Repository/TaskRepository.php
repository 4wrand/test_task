<?php

namespace Repository;

use Core\Doctrine;
use Core\PostMessage;

class TaskRepository
{
    /**
     * Whitelist to protect against sql injection during sorting in the getAllTasksByCriteria() method. 
     * Doctrine has no way of making this request using setParameter() in the orderBy() method
     */
    static private $whiteListForSort = [
        'email' => 'u.email',
        'name' => 'u.firstName',
        'id' => 't.id',
        'completed' => 't.completed', 
    ];

    static private $whiteListForOrder = [
        'asc' => 'ASC',
        'desc' => 'DESC',
    ];


    private function getCountTasks()
    {
        $repo = Doctrine::getEntityManager()->getRepository('\Models\Task');
        $qb = $repo->createQueryBuilder('t');
        $tasks = $qb
            ->select('count(t)')
            ->getQuery()
            ->getSingleScalarResult();

        return $tasks;
    }

    static public function countNumberPages()
    {
        $numberRows = self::getCountTasks();

        $remainder = $numberRows % 3;
        $round = $numberRows - $remainder;

        if ($numberRows <= 3 || $remainder === 0) {
            return $round / 3;
        }

        return $round / 3 + 1;

    }

    private function startingFrom($numberOfPage): string
    {
        return $numberOfPage * 3 - 3;
    }

    static public function getAllTasksByCriteria(string $criteria, string $order, int $numberOfPage)
    {
        $criteria = isset(self::$whiteListForSort[$criteria]) ? self::$whiteListForSort[$criteria] : 't.id';
        $order = isset(self::$whiteListForOrder[$order]) ? self::$whiteListForOrder[$order] : 'ASC';

        $repo = Doctrine::getEntityManager()->getRepository('\Models\Task');
        $qb = $repo->createQueryBuilder('t');
        $query = $qb
            ->select('t')
            ->leftJoin('t.author', 'u')
            ->setFirstResult(self::startingFrom($numberOfPage))
            ->setMaxResults(3)
            ->orderBy($criteria, $order)
            ->getQuery();
        
        try {
            return $query->getResult();
        } catch (\Exception $e) {
            PostMessage::message($e->getMessage(), 'warning');
            return;
        }
        

        return $tasks;
    }
}