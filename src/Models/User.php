<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="users", indexes={@ORM\Index(name="search_users", 
 *      columns={"email", "anonymous"})}
 * )
 */
class User 
{
    /** 
     * @ORM\Id 
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**  
     * @ORM\Column(type="string", length=256)
     */
    protected $email;

    /**  
     * @ORM\Column(name="first_name", type="string", length=64, nullable=true)
     */
    protected $firstName;

    /**  
     * @ORM\Column(name="last_name", type="string", length=64, nullable=true)
     */
    protected $lastName;

    /**  
     * @ORM\Column(type="string", length=32)
     */
    protected $type = 'user';

    /**  
     * @ORM\Column(type="text", length=128, nullable=true)
     */
    protected $password = null;

    /**  
     * @ORM\Column(type="boolean")
     */
    protected $anonymous = true;

    public function getId() : int
    {
        return $this->id;
    }


    public function getEmail() : string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getFirstName() : string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName() : string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    public function getType() : string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function isAnonymous(): bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(bool $anonymous)
    {
        $this->anonymous = $anonymous;
    }
}