<?php

namespace Models;

use Doctrine\ORM\Mapping as ORM;
use Models\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasks")
 */
class Task 
{
    /** 
     * @ORM\Id 
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**  
     * @ORM\Column(type="text")
     */
    protected $content;

    /**  
     * @ORM\Column(type="boolean")
     */
    protected $completed = false;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**  
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $edited = false;

    public function getId()
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content) 
    {
        $this->content = $content;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author) 
    {
        $this->author = $author;
    }

    public function isCompleted(): bool
    {
        return $this->completed;
    }

    public function setCompleted(bool $completed)
    {
        $this->completed = $completed;
    }

    public function isEdited(): bool
    {
        return $this->edited;
    }

    public function setEdited(bool $edited)
    {
        $this->edited = $edited;
    }
}