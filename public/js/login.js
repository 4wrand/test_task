let checkEmail = false;
let checkPass = false;
let emailInput = jQuery('.lamp-input #email-input').get(0);
let passwordInput = jQuery('.lamp-input #password-input').get(0);

jQuery(emailInput).bind('keyup change', function () {
    let lamp = $(this).parent().find('div.lamp');
    let pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if ($(this).val() !== '') {
        checkEmail = true;
        $(lamp).attr('class', 'lamp lamp-light-on');
    } else {
        if (checkEmail === true) {
            $(lamp).attr('class', 'lamp lamp-light-off');
            checkEmail = false;
        }
    }
});

jQuery(passwordInput).bind('keyup change', function () {
    let lamp = $(this).parent().find('div.lamp');
    if ($(this).val() !== '') {
        $(lamp).attr('class', 'lamp lamp-light-on');
        checkPass = true;
    } else {
        if (checkPass === true) {
            $(lamp).attr('class', 'lamp lamp-light-off');
            checkPass = false;
        }
    }
});

jQuery(window).bind('keyup change', function () {
    let $button = $('.form button.button-submit');

    if (checkEmail !== false && checkPass !== false) {
        $button.attr('class', 'button-submit button-active');
        $button.removeAttr('disabled');
    } else {
        $button.attr('class', 'button-submit button-inactive');
        $button.attr('disabled', true);
    }
});