let inputsCheck = {
    'email': false,
    'firstName': false,
    'lastName': false,
    'password': false,
    'confirmPassword': false,
};

let emailInput = jQuery('.lamp-input #email-input').get(0);
let passwordInput = jQuery('.lamp-input #password-input').get(0);
let confirmPasswordInput = jQuery('.lamp-input #confirm-password-input').get(0);
let $firstAndLastNameInputs = jQuery('#first-name-input, #last-name-input');
let allowedTypes = [
    'image/jpeg',
    'image/png',
    'image/gif',
];

function deleteTextFromFakeInput($fakeInput) {
    if ($fakeInput.find('p.file-name')) {
        $fakeInput.find('p.file-name').remove();
    }

    if ($fakeInput.find('p.incorrect-file-extension')) {
        $fakeInput.find('p.incorrect-file-extension').remove();
    }
}

jQuery('.form .form-body input.inputfile').change(function () {
    let $fakeInput = jQuery('.form .form-body div.fake-input');
    let fileName = $(this).val().split('/').pop().split('\\').pop();
    if (this.files.length > 1) {
        deleteTextFromFakeInput($fakeInput);
        $(this).val('');
        $fakeInput.append('<p class="incorrect-file-extension">' + window.multipleUploadMessage + '</p>');
    } else {
        if (allowedTypes.indexOf(this.files[0]['type']) !== -1) {
            deleteTextFromFakeInput($fakeInput);
            fileName = fileName.length > 40 ? fileName.slice(0, 18) + '...' + fileName.slice(-18) : fileName;
                
            $fakeInput.append('<p class="file-name">' + fileName + '</p>');
        } else {
            deleteTextFromFakeInput($fakeInput);
            $(this).val('');
            $fakeInput.append('<p class="incorrect-file-extension">' + window.incorrectFileExtensionMessage + '</p>');
        }
    }
});

jQuery(emailInput).bind('keyup change', function () {
    let lamp = $(this).parent().find('div.lamp');
    let pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if ($(this).val().match(pattern) !== null) {
        inputsCheck['email'] = true;
        $(this).parents('div.input-block').find('.warning').fadeOut();
        $(lamp).attr('class', 'lamp lamp-light-on');
    } else {
        $(this).parents('div.input-block').find('.warning').fadeIn();
        $(lamp).attr('class', 'lamp lamp-light-off');
        inputsCheck['email'] = false;
    }
});

$firstAndLastNameInputs.bind('keyup change', function () {
    let pattern = /[^a-zA-ZА-Яа-яЁё]/gi;
    let lamp = $(this).parent().find('div.lamp');

    if ($(this).val().length >= 2 && $(this).val().length <=64   && $(this).val().match(pattern) === null) {
        inputsCheck[$(this).attr('name')] = true;
        $(this).parents('div.input-block').find('.warning').fadeOut();
        $(lamp).attr('class', 'lamp lamp-light-on');
    } else {
        inputsCheck[$(this).attr('name')] = false;
        $(this).parents('div.input-block').find('.warning').fadeIn();
        $(lamp).attr('class', 'lamp lamp-light-off');
    }
});

jQuery(passwordInput).bind('keyup change', function () {
    let lamp = $(this).parent().find('div.lamp');
    let pattern = /^\S*(?=\S{8,25})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/;

    if ($(this).val().match(pattern) !== null) {
        inputsCheck['password'] = true;
        $(this).parents('div.input-block').find('.warning').fadeOut();
        $(lamp).attr('class', 'lamp lamp-light-on');
    }  else {
        inputsCheck['password'] = false;
        $(this).parents('div.input-block').find('.warning').fadeIn();
        $(lamp).attr('class', 'lamp lamp-light-off');
    }
});

jQuery(confirmPasswordInput).bind('keyup change', function () {
    let lamp = $(this).parent().find('div.lamp');
    let pattern = /^\S*(?=\S{8,25})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/;

    if ($(passwordInput).val() === $(confirmPasswordInput).val() && inputsCheck['password'] === true) {
        inputsCheck['confirmPassword'] = true;
        $(lamp).attr('class', 'lamp lamp-light-on');
        $(this).parents('div.input-block').find('.warning').fadeOut();
    }  else {
        inputsCheck['confirmPassword'] = false;
        $(lamp).attr('class', 'lamp lamp-light-off');
        $(this).parents('div.input-block').find('.warning').fadeIn();
    }
});
jQuery(window).bind('keyup change', function () {
    let result = true;
    let $button = $('.form button.button-submit');

    for (let key in inputsCheck) {
        if (!inputsCheck[key]) {
            result = false;
        }
    }

    if (result) { 
        $button.attr('class', 'button-submit button-active');
        $button.removeAttr('disabled');
    } else {
        $button.attr('class', 'button-submit button-inactive');
        $button.attr('disabled', true);
    }
});

jQuery('.content-wrapper .content .form .form-body input').blur(function () {
    $('.warning').fadeOut();
});