$('.modal').on('open-modal-create-task', function () {
    $(this).fadeIn();
});

$('.modal').mousedown(function (e) {
    var div = $(".modal-window");
    if (!div.is(e.target)
		    && div.has(e.target).length === 0) {
                $('.modal').fadeOut(); 
		}
});

$(window).keydown(function (e) {
    if(e.key === "Escape") {
        $('.modal').fadeOut();
    }
});


