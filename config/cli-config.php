<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

use Core\Doctrine;

$entityManager = Doctrine::getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
