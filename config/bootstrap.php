<?php 

use Core\Doctrine;

require_once __DIR__.'/../core/consts.php';

if(file_exists(dirname(__FILE__) . '/../vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/../vendor/autoload.php';
}

$entityManager = Doctrine::getEntityManager();
