<?php 

namespace Core\Controller;

use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Core\TwigExtension\TwigFunction as CustomTwigFunction;
use Core\TwigExtension\TwigFilter as CustomTwigFilter;
use Twig\TwigFunction;
use Twig\TwigFilter;
use Core\PostMessage;
use Core\Doctrine;

/**
 * Abstract controller
 */
abstract class AController 
{
    public $authenticatedOnly = false;

    /**
     * Page Rendering Method
     * 
     * args: (string) $template - The path to the template
     *       (array) $args - Variables to be sent to the template
     * 
     * return: void
     */
    protected function view(string $tempalate, array $args = []) 
    { 
        $loader = new FilesystemLoader([
            BASE_DIR . '/views',
            BASE_DIR . '/views/base',
            BASE_DIR . '/views/base/include',
            ]);
        $twig = new Environment($loader, [
            'debug' => true,
        ]);
        
        $twig->addExtension(new DebugExtension());
        $this->registerTwigExtensions($loader, $twig);

        $defaultVariable = [
            'baseDir' => BASE_DIR,
            'isAuth'  => !empty($_SESSION['isAuth']) ? $_SESSION['isAuth'] : null,
        ];

        if (!empty($_SESSION['postMessages'])) {
            $defaultVariable['postMessages'] = $_SESSION['postMessages'];
            PostMessage::deleteMessages();
        }

        if (!empty($_SESSION['isAuth']) && !empty($_SESSION['userId'])) {
            $userRepository = Doctrine::getEntityManager()->getRepository('\Models\User');
            $defaultVariable['user'] = $userRepository->find($_SESSION['userId']);
        }

        $args = array_merge($defaultVariable, $args);

        $twig->display($tempalate, $args);
    }

    /**
     * Include of user functions for twig. See app\Core\TwigExtension
     * 
     * args: (FilesystemLoader) $loader
     *       (Environment) $twig
     * 
     * return: void  
     */
    private function registerTwigExtensions(FilesystemLoader $loader, Environment $twig) 
    {
        $twigFunctionObj = new CustomTwigFunction();
        $twigFilterObj = new CustomTwigFilter();

        $twigFunctions = get_class_methods($twigFunctionObj);
        $twigFilters =  get_class_methods($twigFilterObj);

        foreach ($twigFunctions as $twigFunction) {
            $function = new TwigFunction($twigFunction, [$twigFunctionObj, $twigFunction]);
            $twig->addFunction($function);
        }

        foreach ($twigFilters as $twigFilter) {
            $filter = new TwigFilter($twigFilter, [$twigFilterObj, $twigFilter]);
            $twig->addFilter($filter);
        }
    }
}