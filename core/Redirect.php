<?php

namespace Core;

use Controllers\ErrorController;
use Core\Config;

/**
 * Class for working with redirects
 */
class Redirect 
{
    /**
     * The method redirects back to the URL from which the request came
     * 
     * args: empty
     * 
     * return: void
     */
    static public function redirectToBack()
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

     /**
     * The method redirects to the URL from which the request came
     * 
     * args: (string) $path - URL to which you must go
     * 
     * return: void
     */
    static public function redirectTo(string $path = '')
    {
        $config = Config::getInstance();

        header('Location: ' . $config->getHost() . $path);
    }

    /**
     * The method redirects on 404 page
     * 
     * args: empty
     * 
     * return: void
     */
    static public function redirectOnError404() 
    {
        header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', false, 404);
        $page = new ErrorController();
        $page->error404();
    }
}