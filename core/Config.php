<?php

namespace Core;

/**
 * Class for working with configs
 */
class Config 
{
    static protected $instance = null;

    private $configs;

    private $databaseConfigs = [];
    private $host;
    private $lang;

    protected function __construct() 
    {
        $config = $this->getConfigs();

        $this->databaseConfigs = $config['database'] ?: null;
        $this->host = $config['host']['DOMAIN'] . $config['host']['HOST_DIR'];
        // $this->host = $config['host']['HOST_DIR'];
    }

    /**
     * Implementation of the Singleton pattern
     */
    static function getInstance() 
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
 
        return self::$instance;
    }

    public function getDatabaseConfig() 
    {
        return $this->databaseConfigs;
    }

    public function getHost() 
    {
        return $this->host;
    }

    /**
     * 
     */
    private function getConfigs() 
    {
        if ($this->configs) {
            return $this->configs;
        } else {
            try {
                if (file_exists(BASE_DIR . '/config.ini')) {
                    $config = parse_ini_file(BASE_DIR . '/config.ini', true);
                    return $config ?: null;
                } else {
                    throw new \Exception('<b>Error:</b> File <b>config.ini</b> not found'); 
                }
            } catch (\Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }
    }
}
