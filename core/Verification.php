<?php

namespace Core;

class Verification
{
    static public function checkEmail(string $email): bool
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } 

        return false;
    }

    static public function checkUserName(string $userName): bool
    {
        if (!preg_match('/[^a-zA-ZА-Яа-яЁё]/ui', $userName) && mb_strlen($userName) >= 2) {
            return true;
        }
           
        return false;
    }

    static public function checkPassword(string $password): bool
    {
        if (preg_match('/^\S*(?=\S{8,25})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/', $password)) {
            return true;
        }

        return false;
    }
}