<?php

namespace Core\TwigExtension;

use Core\Config;

class TwigFunction
{
    public function path($text) 
    {
        $host = Config::getInstance()->getHost();
        $result = $host . $text;
        return (string) $result;
    }
}