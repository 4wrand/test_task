<?php

namespace Core\TwigExtension;

class TwigFilter 
{
    private $langVariables = null;

    public function t($text) 
    {
        if (!$this->langVariables) {
            $json = file_get_contents( BASE_DIR . DIRECTORY_SEPARATOR . 'langs.json' );
            $this->langVariables = json_decode($json, true);
        }

        $defaultLang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $lang = !empty($_SESSION['lang']) ? $_SESSION['lang'] : $defaultLang;

        return isset($this->langVariables[$lang][$text]) ? $this->langVariables[$lang][$text] : $text;
    }
}