<?php

namespace Core;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Core\Config;

class Doctrine
{
    static protected $instance = null;

    static protected $entityManager;

    protected function __construct() 
    {
        $dbConfigs = Config::getInstance()->getDatabaseConfig();

        $isDevMode = true;
        $proxyDir = null;
        $cache = null;
        $useSimpleAnnotationReader = false;
        $doctrineConfig = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/../src/Models'], $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

        $connection = array(
            'dbname' => $dbConfigs['DATABASE'],
            'user'   => $dbConfigs['USER'],
            'password' => $dbConfigs['PASSWORD'],
            'host' => $dbConfigs['HOST'],
            'driver' => $dbConfigs['DRIVER'],
        );

        self::$entityManager = EntityManager::create($connection, $doctrineConfig);
    }

    /**
     * Implementation of the Singleton pattern
     */
    static function getInstance() 
    {
        if(is_null(self::$instance)) {
            self::$instance = new self();
        }
 
        return self::$instance;
    }

    static function getEntityManager() 
    {
        if(is_null(self::$instance)) {
            self::getInstance();
        }
 
        return self::$entityManager;
    }
}