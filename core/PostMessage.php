<?php

namespace Core;

/**
 * Class for working with post messages. See app/Core/Controller/AController method "view"
 */
class PostMessage 
{
    static private $allowedTypes = [
        'warning',
        'successful'
    ];

    /**
     * Method for initializing a post message
     * 
     * args: (string) $message - Message body
     *       (string) $type - Message type
     * 
     * return: void
     */
    static public function message(string $message, string $type)
    {
        if (in_array($type, self::$allowedTypes)) {
            $_SESSION['postMessages'][$type][] = $message;
        } else {
            echo 'non-existent message type: ' . $type;
            die;
        }      
    }

    /**
     * Method for deleting all post messages
     * 
     * args: empty
     * 
     * retrun: void
     */
    static public function deleteMessages()
    {
        unset($_SESSION['postMessages']);
    }
}