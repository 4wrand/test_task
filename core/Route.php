<?php 

namespace Core;

use Controllers;
use Core\Redirect;

/**
 * Routing class
 */
class Route 
{
    /**
     * Url processing start
     * 
     * args: empty
     * 
     * return: void
     */
    static public function start() 
    {
        $requestUri = null;
        $hostDir = preg_replace('/\/index.php/', '', $_SERVER['SCRIPT_NAME']);

        if ($hostDir) {
            $hostDir = str_replace('/', '\/', $hostDir);

            $requestUri = preg_replace('/' . $hostDir . '\//', '', $_SERVER['REQUEST_URI']); 
        } else {
            $requestUri = $_SERVER['REQUEST_URI'];
        }

        $routes = parse_url($requestUri);

        $controller = 'Controllers\TaskController';
        $action = !empty($_GET['action']) ? $_GET['action'] : 'default';
        
        if (!empty($routes['path'])) {
            if ($routes['path'] !== '/') {
                $controllerName = str_replace('/', '', $routes['path']);
                $controller = 'Controllers\\' . ucfirst($controllerName) . 'Controller';
            } 
        }
        
        if (class_exists($controller) && method_exists($controller, $action)) {
            $object = new $controller();

            if ($object->authenticatedOnly === true && !(isset($_SESSION['isAuth']) && $_SESSION['isAuth'] === true)) {
                Redirect::redirectTo('auth');
            }

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                call_user_func([$object, $action], $_POST);
            }

            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                call_user_func([$object, $action]);
            }
        } else {
            Redirect::redirectOnError404();
        }

    }
}